module gitee.com/nuokwan_backend_group/cli

go 1.13

require (
	github.com/mitchellh/mapstructure v1.2.2
	github.com/urfave/cli/v2 v2.2.0
)
